'use strict';

var map;
function initMap(latLngA, latLngB) {
  var mapOptions = {
      center: new google.maps.LatLng(latLngA, latLngB),
      zoom: 7 ,
      // disableDefaultUI: true,
      scrollwheel: false,
      styles: [
          {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#e9e9e9"
                  },
                  {
                      "lightness": 17
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#f5f5f5"
                  },
                  {
                      "lightness": 20
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 17
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "geometry.stroke",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 29
                  },
                  {
                      "weight": 0.2
                  }
              ]
          },
          {
              "featureType": "road.arterial",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 18
                  }
              ]
          },
          {
              "featureType": "road.local",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 16
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#f5f5f5"
                  },
                  {
                      "lightness": 21
                  }
              ]
          },
          {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#dedede"
                  },
                  {
                      "lightness": 21
                  }
              ]
          },
          {
              "elementType": "labels.text.stroke",
              "stylers": [
                  {
                      "visibility": "on"
                  },
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 16
                  }
              ]
          },
          {
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "saturation": 36
                  },
                  {
                      "color": "#333333"
                  },
                  {
                      "lightness": 40
                  }
              ]
          },
          {
              "elementType": "labels.icon",
              "stylers": [
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#f2f2f2"
                  },
                  {
                      "lightness": 19
                  }
              ]
          },
          {
              "featureType": "administrative",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#fefefe"
                  },
                  {
                      "lightness": 20
                  }
              ]
          },
          {
              "featureType": "administrative",
              "elementType": "geometry.stroke",
              "stylers": [
                  {
                      "color": "#fefefe"
                  },
                  {
                      "lightness": 17
                  },
                  {
                      "weight": 1.2
                  }
              ]
          }
      ]
  };

  var markers = [
    {
      "title": "Краснодар",
      "latLng": new google.maps.LatLng(45.0392674,38.987221),
      "index": 1,
      "icon": "default"
    },
    {
      "title": "Армавир",
      "latLng": new google.maps.LatLng(44.9873603,41.1111326),
      "index": 1,
      "icon": "default"
    },
    {
      "title": "Кропоткин",
      "latLng": new google.maps.LatLng(45.4296049,40.5540351),
      "index": 1,
      "icon": "default"
    },
    {
      "title": "Новокубанск",
      "latLng": new google.maps.LatLng(45.111673,41.0182824),
      "index": 1,
      "icon": "default"
    },
    {
      "title": "Ставрополь",
      "latLng": new google.maps.LatLng(45.0454764,41.9683431),
      "index": 1,
      "icon": "default"
    }
  ];

  var icons = {
    "default": new google.maps.MarkerImage('img/metka.png',
        new google.maps.Size(16,16),
        new google.maps.Point(0,0),
        new google.maps.Point(8,8))
  };

  // создаем карту внутри элемента #google-map
  map = new google.maps.Map(document.getElementById("map"), mapOptions);

  var setMarkers = function (map) {
    var i = 0, markerList = [];
    var addMarker = setInterval(function () {
        markerList.push(new google.maps.Marker({
            //animation: google.maps.Animation.DROP,
            position: markers[i].latLng,
            map: map,
            icon: icons[markers[i].icon],
            title: markers[i].title,
            zIndex: markers[i].index
        }));
        attachContent(markerList[i], markers[i].title);
        i++;
        if (i == markers.length) {
            clearInterval(addMarker);
        }
    }, 0);
  };

  function attachContent(marker, content) {
    var infowindow = new google.maps.InfoWindow({
        content: '<div class="marker-info">' + content + '</div>'
    });

    marker.addListener('click', function() {
        infowindow.open(marker.get('map'), marker);
    });
  }

  setMarkers(map);

}

// map.setCenter();

var btiApp = angular.module('btiApp', ['ngRoute', 'ngSanitize']);

btiApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  //$locationProvider.html5Mode({
  //  enabled: true,
  //  requireBase: false
  //});
  $routeProvider
    .when('/',{
      templateUrl: 'templates/home.html'
    })
    .when('/about',{
      templateUrl: 'templates/about.html',
      controller: 'AboutCtrl'
    })
    .when('/services',{
      templateUrl: 'templates/services.html',
      controller: 'ServiceCtrl'
    })
    .when('/services/:serviceId',{
      templateUrl: 'templates/service_detail.html',
      controller: 'ServiceDetailCtrl'
    })
    .when('/news',{
      templateUrl: 'templates/news.html',
      controller: 'NewsCtrl'
    })
    .when('/news/:newId',{
      templateUrl: 'templates/new_detail.html',
      controller: 'NewDetailCtrl'
    })
    .when('/faq',{
      templateUrl: 'templates/faq.html',
      controller: 'FaqCtrl'
    })
    .when('/application',{
      templateUrl: 'templates/application.html',
      controller: 'ApplicationCtrl'
    })
    .when('/notice',{
      templateUrl: 'templates/notice.html',
      controller: 'NoticeCtrl'
    })
    .when('/notice/:newId',{
      templateUrl: 'templates/notice_detail.html',
      controller: 'NoticeDetailCtrl'
    })
    .when('/contacts',{
      templateUrl: 'templates/contacts.html',
      controller: 'ContactsCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);

btiApp.controller('NavCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.navigation = [
    {
      title: "Услуги",
      url: "#/services"
    },
    {
      title: "Новости",
      url: "#/news"
    },
    {
      title: "Вопрос-ответ",
      url: "#/faq"
    },
    {
      title: "О компании",
      url: "#/about"
    },
    {
      title: "Контакты",
      url: "#/contacts"
    }
  ];
}]);

btiApp.controller('MainCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {

  $scope.url = $location.path();

  if($location.path() === '/' || $location.path() === '') {
    $scope.showSlider = true;
  } else {
    $scope.showSlider = false;
  }

  $scope.changePage = function(url) {
    $scope.url = url;
    $scope.showSlider = url === '/' ? true : false;
  };

  $scope.$watch('url', function(newValue, oldValue) {
    console.log(newValue);
    // if(newValue == '/') {
      $(window).on('load', function() {
        initMap(45.0392674,38.987221);
      });
    // }
  });

  $scope.showMenu = false;
  $scope.burger = function() {
    $scope.showMenu = !$scope.showMenu;
  };


}]);

btiApp.controller('SliderCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.slides = [
    {
      id: 1,
      title: "Кадастровые работы",
      content: "Инвентаризация объектов капитального строительства, в том числе незавершенных строительством объектов, технический учет и изготовление справок"
    },
    {
      id: 2,
      title: "Геодезия и картография",
      content: "Инвентаризация объектов капитального строительства, в том числе незавершенных строительством объектов, технический учет и изготовление справок"
    },
    {
      id: 3,
      title: "Техническая инвентаризация",
      content: "Инвентаризация объектов капитального строительства, в том числе незавершенных строительством объектов, технический учет и изготовление справок"
    }
  ];

  $scope.currentSlide = 1;

  $scope.setSlide = function(event,slide) {
    event.preventDefault();
    return $scope.currentSlide = slide;
  };

}]);

/* услуги */
btiApp.controller('ServiceCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {

  $scope.title = "Услуги";

  $scope.serviceType = "direction";

  $scope.changeServiceType = function(event,type) {
    event.preventDefault();
    return $scope.serviceType = type;
  };

  $http.get('json/services.json').success(function(data) {
    $scope.services = data;
  }).error(function() {
    console.log('Произошла неудача, при выполнение запроса');
  });

}]);

/* детальная страница услуги */
btiApp.controller('ServiceDetailCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {

  $scope.serviceId = $routeParams.serviceId;
  var url = 'json/' + $routeParams.serviceId + '/.json';

  $http.get('json/services.json').success(function(data) {
    $scope.service = data;
  }).error(function() {
    console.log('Произошла неудача, при выполнение запроса');
  });

}]);

/* faq */
btiApp.controller('FaqCtrl', function($scope, $http) {

  $scope.title = "Часто задаваемые вопросы";

  $http.get('json/faq.json').success(function(data) {
    $scope.faqList = data;
  }).error(function() {
    console.log('Произошла неудача, при выполнение запроса');
  });

  $scope.toggler = function(index) {
    console.log(index);
  }; 

});

/* новости */
btiApp.controller('NewsCtrl', function($scope, $http) {

  $scope.title = "Новости";

  $http.get('json/news.json').success(function(data) {
    $scope.newsList = data;
  }).error(function() {
    console.log('Произошла неудача, при выполнение запроса');
  });

});

/* детальная страница новости */
btiApp.controller('NewDetailCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  
}]);

/* онлайн-заявка */
btiApp.controller('ApplicationCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.title = "Онлайн-заявка";
  $scope.current = 1;

  $scope.nextStep = function(step) {
    console.log(step)
    $scope.current++;
  };

  $scope.prevStep = function(step) {
    console.log(step)
    $scope.current--;
  };

  $scope.setStep = function(step) {
    if(step > $scope.current) {
      console.log('Next step');
      $scope.current++;
    } else {
      console.log('Prev step')
      $scope.current--;
    }
  };
}]);

/* уведомления */
btiApp.controller('NoticeCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.title = "Уведомления";

  $scope.showSlider = false;

  $scope.noticeType = "";

  $scope.changeNoticeType = function(event,type) {
    event.preventDefault();
    return $scope.noticeType = type;
  };

  $http.get('json/notice.json').success(function(data) {
    $scope.noticeList = data;
  }).error(function() {
    console.log('Произошла неудача, при выполнение запроса');
  });

}]);

/* детальная страница уведомлялки */
btiApp.controller('NoticeDetailCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.title = "Уведомления";

  $scope.showHistory = false;

  $scope.toggler = function() {
    $scope.showHistory = !$scope.showHistory;
  };

}]);

/* о компании */
btiApp.controller('AboutCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.title = "О компании";


}]);

/* контакты */
btiApp.controller('ContactsCtrl', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.title = "Контакты";

  $(window).on('load', function() {
        initMap(45.0392674,38.987221);
      });

}]);
