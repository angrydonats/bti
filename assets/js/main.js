function extend( a, b ) {
  for( var key in b ) {
      if( b.hasOwnProperty( key ) ) {
          a[key] = b[key];
      }
  }
  return a;
}



$(function() {

  // $(window).on('load', function() {
  //   initMap();
  // });

  // $('#main-slider').slick({
  //   autoplay: false,
  //   dots: false,
  //   fade: true,
  //   infinite: true,
  //   slidesToShow: 1,
  //   slidesToScroll: 1
  // });

  /* прокрутка к секциям */
    jumperCtrl = $('.jumper');

    jumperCtrl.on('click', function(e) {
        e.preventDefault();

        var anchor = $(this).data('anchor'),
          target = $('#' + anchor).offset().top;

        $("html:not(:animated),body:not(:animated)").animate({scrollTop: target}, 1100);

        return false;
    });


  /* блок на слайдере */
  var panel = $('.js-object_panel'),
    panelControl = panel.find('a');

  panelControl.on('click', function(ev) {
    ev.preventDefault();

    console.log($(this).data('type'))
  });


  /* переключалка вариантов приобретения */
  var switcherOrder = $('.js-switcher_order'),
      switcherOrderControl = switcherOrder.find('.switcher__control'),
      switcherOrderEl = switcherOrder.find('.switcher__link');

  switcherOrderEl.on('click', function(ev) {
    ev.preventDefault();

    switcherOrderControl.removeClass('_active');
    $(this).parent().addClass('_active');
  });


  /* яндекс карта */
  //ymaps.ready(initMap);
  //var myMap,
  //   myPlacemark;
  //
  //function initMap() {
  //   myMap = new ymaps.Map("map", {
  //       center: [45.02285171, 38.97226401],
  //       zoom: 18,
  //       controls: []
  //   });
  //
  //   myPlacemark = new ymaps.Placemark([45.02285171, 38.97226401], {
  //       hintContent: 'КПД Инвест',
  //       balloonContent: 'улица Красноармейская 36'
  //   });
  //   myMap.geoObjects.add(myPlacemark);
  //}

  // эффекты для инпутов
  if (!String.prototype.trim) {
			(function() {
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, '');
				};
			})();
	}

	[].slice.call( document.querySelectorAll( 'input.form__input' ) ).forEach(function( inputEl ) {
		if( inputEl.value.trim() !== '' ) {
			classie.add( inputEl.parentNode, 'form__item--focus' );
		}

		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	});

	function onInputFocus( ev ) {
		classie.add( ev.target.parentNode, 'form__item--focus' );
	};

	function onInputBlur( ev ) {
		if( ev.target.value.trim() === '' ) {
			classie.remove( ev.target.parentNode, 'form__item--focus' );
		}
	};


  /* модальные окна */
  var $modal = $('.modal');
  $('.js-md-show').on('click', function (e) {
        e.preventDefault();

        var that = $(this);

        var type = that.data('type'),
          source = that.data('source');

        $modal.removeClass('_show');
        $.each($modal, function() {
          if ( $(this).data('type') == type ) {
            $(this).addClass('_show');
          }
        });
    });

    $('.md-close').on('click', function (e) {
        e.preventDefault();

        $modal.removeClass('_show');
    });


    /* fancybox */
    //$('.photobox__item a').fancybox({
    //    closeBtn  : true,
    //    arrows    : true,
    //    nextClick : true,
    //
    //    helpers : {
    //        thumbs : {
    //            width  : 50,
    //            height : 50
    //        }
    //    }
    //});

});
